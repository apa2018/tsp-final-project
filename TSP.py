#!/usr/bin/env python
# coding: utf-8

# In[1]:


# get_ipython().run_line_magic('matplotlib', 'notebook')
import os
import numpy as np
import pandas as pd
import random
from math import sqrt
import time
from IPython.display import display


# In[2]:


def writeResultsToFile(verticesPicked, pathCost, nbOfIterations, filename):
  resultAsText = 'Order of vertices picked: \n'
  for el in verticesPicked:
    resultAsText += str(el[0]) + ', '
  resultAsText = resultAsText[:-2]
  resultAsText += '\nTotal cost: \n' + str(pathCost)
  resultAsText += '\nNumber of iterations: \n' + str(nbOfIterations)

  with open(filename, 'w') as f:
    f.write(resultAsText)


def readFile(path):
    with open(path, "r") as f:
        return f.read().split("\n")[3:-1]


def parseText(text):
  coordenadas = []
  for vertice in text:
      if vertice != 'EOF':
        formattedVertice = vertice.replace('   ', ' ').replace('  ', ' ').replace(
            '    ', ' ').replace('     ', ' ').replace('      ', ' ')
        coordenadas.append(list(map(float, formattedVertice.split())))
  return fixVertices(coordenadas)


def fixVertices(coordenadas):
  pontos = []
  for vertice in coordenadas:
      pontos.append([int(vertice[0]), vertice[1], vertice[2]])
  return pontos


def exec_time(start, end):
  segundos = end - start
  minutos = segundos/60

  return (minutos, segundos)

# In[3]:


memoisedCost = [[]]


def eraseMemoisedMatrix(size):
  global memoisedCost
  memoisedCost = np.zeros((size, size), dtype=int)


# # Cálculo das distâncias

# In[4]:


def dist(a, b):
    if memoisedCost[a[0], b[0]] == 0:
        cost = round(sqrt(pow(a[1] - b[1], 2) + pow(a[2] - b[2], 2)))
        memoisedCost[a[0], b[0]] = cost

    return memoisedCost[a[0], b[0]]


def distancia_percurso(points):
    # Caso em que a lista é zero
    if len(points) == 0:
        return 0

    distanciaTotalPercurso = 0

    # Soma todos os pontos
    for i in range(len(points) - 1):
        distanciaTotalPercurso += dist(points[i], points[i + 1])

    # Soma a distância do último ponto com o primeiro
    distanciaTotalPercurso += dist(points[len(points)-1], points[0])
    return distanciaTotalPercurso


# # Heuristica de Construção
# ## Método escolhido: Nearest Neighbor

# In[5]:


def nearestNeighbor(points):
    pontos = list(points)

    if len(pontos) == 0:
        return []

    # Escolhe o vértice inicial
    atual = pontos[0]

    # Armazena em um array que terá o caminho criado pelos pontos_escolhidos
    pontos_escolhidos = [atual]
    # Remove pois já escolhi
    pontos.remove(atual)
    # Repete até que o conjunto inicial acabe
    while len(pontos) > 0:
        ponto_de_comparacao = pontos[0]
        # Escolhe um vértice mais próximo
        for point in pontos:
            if dist(atual, point) < dist(atual, ponto_de_comparacao):
                ponto_de_comparacao = point
        # Acrescenta o vértice no array
        pontos_escolhidos.append(ponto_de_comparacao)
        pontos.remove(ponto_de_comparacao)
        # agora parto do nó escolhido
        atual = ponto_de_comparacao
    return pontos_escolhidos, distancia_percurso(pontos_escolhidos)


# # Movimentos de Vizinhança
# ## Heurísticas escolhidas:
# - 2-OPT
# - firstImprovement

# In[6]:


def twoOpt(points):
  pontos = list(points)

  melhor_caminho = list(pontos)
  melhor_distancia = distancia_percurso(melhor_caminho)

  for i in range(len(pontos) - 1):
    for j in range(i + 2, len(pontos) - 1):   # i + 2 para haver ao menos uma troca
      # Verifica se a troca de 2 arestas é vantajosa, se sim, troque pela menor
      if dist(pontos[i], pontos[i+1]) + dist(pontos[j], pontos[j+1]) > dist(pontos[i], pontos[j]) + dist(pontos[i+1], pontos[j+1]):
        # É um Mirror/Swap from i+1 to j+1
        pontos[i+1:j+1] = reversed(pontos[i+1:j+1])
        nova_distancia = distancia_percurso(pontos)
        if nova_distancia < melhor_distancia:
          #Encontrei um caminho melhor
          melhor_caminho = pontos
          melhor_distancia = nova_distancia
        else:
            # RESWAP pois não é o melhor até agora
            pontos[i+1:j+1] = reversed(pontos[i+1:j+1])

  return melhor_caminho, melhor_distancia


# In[7]:


def firstImprovement(array_lista):

    if len(array_lista) < 0:
        raise ValueError("Lista vazia")

    lista = list(array_lista)
    melhor_ate_agora = distancia_percurso(lista)

    for i in range(len(lista)):
        a = lista.pop(i)
        for j in range(len(lista)+1):
            lista.insert(j, a)
            nova_distancia = distancia_percurso(lista)
            if nova_distancia < melhor_ate_agora:
                melhor_ate_agora = nova_distancia

                #Se eu comentar essa linha o algoritmo se torna um Best Improvement
                # , tendo que visitar toda a vizinhança para determinar o melhor
                return lista, melhor_ate_agora
            else:
                # Removendo, já que não possui distância menor
                lista.pop(j)
        lista.insert(i, a)
    return lista, melhor_ate_agora


# # Variable Neighbourhood Descent (VND)
# ## Varia a vizinhança de x
# Parte do pressuposto que **um ótimo local com relação a uma vizinhança não necessariamente corresponde a um ótimo com relação a outra vizinhança**.
#

# In[8]:


def vnd(array, nbIterations):
    points = list(array)

    melhor_ate_agora = distancia_percurso(points)
    print("Custo do caminho inicial: ", melhor_ate_agora)
    print("Numero de iteracoes: ", nbIterations)
    while nbIterations > 0:

        # Se um for ruim, rode o outro
        lista_twoOpt, twoOpt_dist = twoOpt(points)
        if twoOpt_dist < melhor_ate_agora:
            melhor_ate_agora = twoOpt_dist
            points = lista_twoOpt
            nbIterations -= 1
            continue
            
        #print('OPT ruim, tentando o First Search')
        fi_array, fi_custo = firstImprovement(points)
        #print("Insertion:", fi_custo, len(fi_array))
        if fi_custo < melhor_ate_agora:
            melhor_ate_agora = fi_custo
            points = fi_array
            nbIterations -= 1
            continue

        # Caso o firstImprovement e o 2-OPT forem ruim, pare
        print("firstImprovement e 2-OPT sem sucesso")
        break
    print('VND finalizado', melhor_ate_agora, '\n')
    return points, melhor_ate_agora


#%% [markdown]

# # GRASP

#%%


def construcao_solucao(alfa, pontos):
    caminho = list(pontos)
    if len(caminho) == 0:
        return 0

    # Escolhe o vértice inicial
    current = caminho[0]
    # Armazena em um array, que terá o caminho criado pelo nnpoints
    solucao = [current]

    # Remove do conjuto total
    caminho.remove(current)

    # Repete até que o conjunto inicial acabe
    while len(caminho) > 0:

        # Escolhe um nó para ser analisado
        vertice = caminho[0]

        # Criar lista de candidatos, com base nos outros que não foram visitados
        if len(caminho) >= 2:
            lista_candidatos = list(caminho[1:])

        if len(caminho) == 2:
            # Acrescenta o vértice no array
            solucao.append(vertice)
            caminho.remove(vertice)
        elif len(caminho) == 1:
            # Acrescenta o vértice no array
            solucao.append(vertice)

        # Lista que armazenará os candidatos em ordem crescente de distancia do vertice
        lista_candidatos_restrita = []

        # Criar lista de candidatos ordenada

        # Para cada candidato na lista_candidatos:
        for candidato in lista_candidatos:

          # Calcule a distancia do nó atual aos outros
          distancia_candidato_vertice = dist(vertice, candidato)

          # Adicione na lista_candidatos_restrita a lista [indice, x, y, distancia_candidato_vertice]
          lista_candidatos_restrita.append(
              (*candidato, distancia_candidato_vertice))

        # Ordene pela coluna das distancias
        lista_candidatos_restrita.sort(key=lambda x: x[3])

        # Escolher um candidato usando a função aleatória
        indice_valor = random.randrange(
            0, int(alfa*len(lista_candidatos_restrita)+1))
        vertice = [int(lista_candidatos_restrita[indice_valor][0]),
                   lista_candidatos_restrita[indice_valor][1], lista_candidatos_restrita[indice_valor][2]]

        # Acrescenta o vértice no array
        solucao.append(vertice)

        # Remove do conjunto
        caminho.remove(vertice)

        # Considera que o novo nó a ser utilizado será o do final
        current = vertice
    return solucao


def GRASP(N_iteracoes_GRASP, N_iteracoes_VND, alfa, pontos):
  melhores_solucoes = []
  duracoes_parciais = []
  for i in range(N_iteracoes_GRASP):
      
      start = time.time()
      constPontos = construcao_solucao(alfa, pontos)

      # "busca_local"
      solucao = vnd(constPontos, N_iteracoes_VND)
      # [0] para minutos e [1] para segundos
      duracoes_parciais.append(exec_time(start=start, end=time.time())[1])

      # Memoriza melhor solucao
      melhores_solucoes.append(solucao)
  return melhores_solucoes, duracoes_parciais

# # Iniciando o algoritmo


# In[10]:
#solucoes otimas pre-calculadas
solucoes_otimas = {'ch130': 6110, 'berlin52': 7542, 'bier127': 118282}

colunas = ['ótimo', 'média solução', 'melhor solução', 'média tempo', 'gap']

constTable = pd.DataFrame(columns=colunas)      #tabela do construtivo
graspTable = pd.DataFrame(columns=colunas)      #tabela da heuristica

#funcao que retorna uma lista com os arquivos do diretorio escolhido
instances = os.listdir("./instancias_teste/")       
for instance in instances:
  file_path = "./instancias_teste/" + instance
  if not instance[0] == '$':  # lendo apenas as bases selecionadas
    continue
  text = readFile(file_path)
  vertices = parseText(text)

  # $berlin52.txt   -->   berlin52
  nome_formatado = instance[1:].split('.')[0]
  solucao_otima = solucoes_otimas[nome_formatado]

  # Rodando o construtivo
  for i in range(1):  # executa 1x pois é determinístico, logo daria o mesmo resultado rodando n vezes

    melhores_parciais_const = []
    duracoes_parciais_const = []
    nb_iteracoes_vnd = 10           #executo o vnd 10x

    eraseMemoisedMatrix(len(vertices))

    constStart = time.time()  # inicio o temporizador

    nn_caminho, nn_custo = nearestNeighbor(vertices)
    vnd_results, melhor_caminho_vnd = vnd(nn_caminho, nb_iteracoes_vnd)

    melhores_parciais_const.append(melhor_caminho_vnd)
    # [0] para minutos e [1] para segundos
    duracoes_parciais_const.append(exec_time(start=constStart, end=time.time())[1])  #paro o temporizador
    gap_formatado_const = "{0:.2f}%".format(
        ((min(melhores_parciais_const) - solucao_otima)/solucao_otima)*100)


    #inserindo na tabela os resultados para um dataset
  constTable.loc[nome_formatado] = {
      'ótimo': solucao_otima,
      'média solução': np.mean(melhores_parciais_const),
      'melhor solução': min(melhores_parciais_const),
      'média tempo': np.mean(duracoes_parciais_const),
      'gap': gap_formatado_const
  }

  # GRASP
  alfa = 0.1

    #rodando o GRASP 10x, vnd 10x e alfa 0.1
  melhores_grasp, duracoes_parciais_grasp = GRASP(10, 10, alfa, vertices)

  melhores_parciais_grasp = [el[1] for el in melhores_grasp]
  gap_formatado_grasp = "{0:.2f}%".format(
      ((min(melhores_parciais_grasp) - solucao_otima)/solucao_otima)*100)

  graspTable.loc[nome_formatado] = {
      'ótimo': solucao_otima,
      'média solução': np.mean(melhores_parciais_grasp),
      'melhor solução': min(melhores_parciais_grasp),
      'média tempo': np.mean(duracoes_parciais_grasp),
      'gap': gap_formatado_grasp
  }
  print('\n\n========================#########========================\n\n')

display(constTable)
display(graspTable)
